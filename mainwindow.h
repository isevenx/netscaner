#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QByteArray>
#include <QNetworkInterface>
#include <QHostAddress>
#include <unistd.h>
#include <QTimer>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    QString minMaxIP(QString ip, QString mask, int minmax);
    bool ifSmbox(QByteArray str);
    ~MainWindow();
protected slots:
    void replyFinished(QNetworkReply *reply);
private:
    Ui::MainWindow *ui;
    QNetworkInterface *inter;
};


class QReplyTimeout : public QObject {
  Q_OBJECT
public:
  QReplyTimeout(QNetworkReply* reply, const unsigned long timeout) : QObject(reply) {
    Q_ASSERT(reply);
    if (reply) {
      QTimer::singleShot(timeout, this, SLOT(timeout()));
    }
  }

private slots:
  void timeout() {
    QNetworkReply* reply = static_cast<QNetworkReply*>(parent());
    if (reply->isRunning()) {

      reply->close();
    }
  }
};

#endif // MAINWINDOW_H
