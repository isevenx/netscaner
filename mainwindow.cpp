#include "mainwindow.h"
#include "ui_mainwindow.h"


bool MainWindow::ifSmbox(QByteArray str)
{
    if (str.startsWith("SmBox")) return true;
    else return false;
}
void MainWindow::replyFinished(QNetworkReply *reply)
{
    QByteArray str;
    str=reply->readAll();
    if (ifSmbox(str)) ui->listWidget_2->addItem(str);
    reply->deleteLater();
}


QString MainWindow::minMaxIP(QString ip, QString mask,int minmax)
{
    QString bin="",sk="";
    int s[4],m[4],q=0;
    unsigned int iip,imask;

    ip.append('.');
    for (int i=0;i<ip.length();i++){
        if (ip[i]=='.'){
            s[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(ip[i]);
    }
    ip.chop(1);
    q=0;
    mask.append('.');
    for (int i=0;i<mask.length();i++){
        if (mask[i]=='.'){
            m[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(mask[i]);
    }
    mask.chop(1);
    iip = s[3] | (s[2]<<  8)  | (s[1] << 16 ) | (s[0] << 24 );
    imask = m[3] | (m[2] << 8) | (m[1] << 16 ) | (m[0] << 24 );
    if (minmax==0) iip = iip & imask;
    else {
        imask = ~imask;
        iip = iip | imask;
    }
    s[0]=iip >> 24 ;
    s[1]=(iip >> 16 )&255;
    s[2]=(iip >> 8)&255;
    s[3]=(iip & 255);
    sk=sk+QString::number(s[0])+'.'+QString::number(s[1])+'.'+QString::number(s[2])+'.'+QString::number(s[3]);

    return sk;
}


//===================================================================================================================================================


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QList<QHostAddress> listIpAddrs;
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
    QList<QNetworkInterface> listInterNames;
    inter=new QNetworkInterface();
    listIpAddrs = inter->allAddresses();
    listInterNames = inter->allInterfaces();
    QString str;

    ui->listWidget->addItem(new QListWidgetItem("IP addresses:") );
    for (int i = 0; i < listInterNames.size(); i++)
       {
        QNetworkInterface iface = ifaces.at(i);
            ui->listWidget->addItem(new QListWidgetItem("Inet: " +iface.addressEntries().at(0).ip().toString()));
            ui->listWidget->addItem(new QListWidgetItem("Netmask: "+iface.addressEntries().at(0).netmask().toString()));
            ui->listWidget->addItem(new QListWidgetItem("broadcast: "+iface.addressEntries().at(0).broadcast().toString()));
            ui->listWidget->addItem(new QListWidgetItem(QString("\n")));

       }
    ui->listWidget->addItem(new QListWidgetItem("Interface names:") );
    for (int i = 0; i < listInterNames.size(); ++i)
       {
       str = listInterNames.at(i).name();  // Show the IP address
       ui->listWidget->addItem(new QListWidgetItem(str));
       }

    //===============================================================================================================================================

    QString ip,dm,sk,ipmax,ipmin,bin;

    QNetworkInterface iface = ifaces.at(1);         //tikla izvele
    ip=iface.addressEntries().at(0).ip().toString();
    dm=iface.addressEntries().at(0).netmask().toString();

    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(ui->listWidget);


    QNetworkRequest request;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
                  this, SLOT(replyFinished(QNetworkReply*)));







    int s[4],z[4],q=0,i,y,t,g;
    ip="127.0.0.0";
    dm="255.0.0.0";
    ipmin=minMaxIP(ip,dm,0);
    ipmax=minMaxIP(ip,dm,1);

    ipmin.append('.');
    ipmax.append('.');
    for (i=0;i<ipmin.length();i++){
        if (ipmin[i]=='.'){
            s[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(ipmin[i]);
    }
    q=0;
    for (i=0;i<ipmax.length();i++){
        if (ipmax[i]=='.'){
            z[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(ipmax[i]);
    }
    ipmin.chop(1);
    ipmax.chop(1);

    ui->listWidget_2->addItem("From:   "+ipmin);
    ui->listWidget_2->addItem("To:         "+ipmax);
    ui->listWidget_2->addItem(" ");



    for (i=s[0];i<=z[0];i++){
        for (y=s[1];y<=z[1];y++){
            for(t=s[2];t<=z[2];t++){
                for(g=s[3];g<=z[3];g++){
                    s[3]=g;
                    sk="http://";
                    sk=sk+QString::number(s[0])+'.'+QString::number(s[1])+'.'+QString::number(s[2])+'.'+QString::number(s[3]);
                    sk=sk+":9898/scanid";
                    request = QNetworkRequest(QUrl(sk));
                    manager->get(request);
                }
            }
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
