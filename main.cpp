#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setStyleSheet("* { background-color:rgb(250,247,205); padding: 7px ; }");
    w.show();

    return a.exec();
}
